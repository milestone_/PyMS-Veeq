## What is this folder for?
In this folder you will store scripts you want to include using ASC3 Preprocessor. You can create subfolders for organizational purposes.

To include a script you will use syntax:<br>
`include script.asc3`<br>
*or*<br>
`include subfolder/script.asc3`

Included scripts can include other scripts. you may want to have a script that includes others.

For example:
`common.asc3`
```
include Common/expansion_utils.asc3
include Zerg/attack_types.asc3
include Zerg/build_orders.asc3
include Protoss/attack_types.asc3
include Protoss/build_orders.asc3
include Terran/attack_types.asc3
include Terran/build_orders.asc3
```
Then you would only `include common.asc3` in your scripts.

### Reserved Filenames
Those filenames are reserved for the use of preprocessor and should not be used by the user.
`input.asc3`
`output.asc3`

`temp.asc3` *only in debug mode*