@echo off
title Installing dependencies
color 07
echo Installing dependencies, please wait...
python -m pip install --upgrade Pillow==6.2.2
python -m pip install --upgrade pyperclip==1.8.0
pause